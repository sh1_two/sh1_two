/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_manage_exec.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/14 02:52:43 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/30 23:32:29 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>
#include <errno.h>

static int	test_signal(pid_t pid, int status)
{
	if (WIFEXITED(status))
		return (WEXITSTATUS(status));
	else if (WIFSIGNALED(status))
		return (WTERMSIG(status));
	else if (WIFSTOPPED(status))
		return (status);
	return (0);
}

int		ft_str(t_env *env, t_btree *l, t_btree *r, t_exec exec)
{
	(void)r;
	return (ft_exec_str(env, l, exec));
}

int		ft_execve(t_env *env, t_btree *tree, char **envp)
{
	if ((tree->error == 1 && tree->operand == o_and_d) ||
			(tree->error == 0 && tree->operand == o_pipe_d))
		return (0);
	if (ft_strcmp(tree->cde_name, "cd") == 0)
		ft_buildin_cd(tree, envp);
	else if (ft_strcmp(tree->cde_name, "env") == 0)
	{
		if (tree->args_tab[1] == NULL || ft_strcmp(tree->args_tab[1], "") == 0)
			ft_print_env(envp);
		else
			ft_printf("%s%s%s%s %s%s\n", BLUE, tree->args_tab[1], C_NONE,
					C_BROWN, ft_get_env(envp, tree->args_tab[1]), C_NONE);
		return (0);
	}
	else if (ft_strcmp(tree->cde_name, "setenv") == 0)
	{
		if (!tree->args_tab[1] && !tree->args_tab[2])
			return (RN_ERR("Setenv need two params!\n"), -1);
		ft_set_env(&envp, tree->args_tab[1], tree->args_tab[2]);
	}
	else if (ft_strcmp(tree->cde_name, "unsetenv") == 0)
		if (ft_unset_env(&envp, tree->args_tab[1]) < 0)
			return (-1);
	ft_printf(C_RED"erreur : %d (%s)\n", errno, strerror(errno));
	return (0);
}

int		ft_exec_str(t_env *env, t_btree *tree, t_exec exec)
{
	pid_t	pid;
	int		status;
	char	*function;

	if ((tree->error >= 1 && tree->operand == o_and_d) ||
			(tree->error == 0 && tree->operand == o_pipe_d))
		return (0);
	if (exec == &execve && !(function = command_path(env, tree->cde_name)))
		return (ft_printf("Cannot find function %s\n", tree->cde_name), -1);
	else if (!tree->args_tab)
		return (ft_printf("Table des arguements vide!"), -1);
	else
	{
		if ((pid = fork()) == 0)
		{
			execve(function, ft_star(env, tree->args_tab), env->envp);
			N_ERR("Cannot execute function %s", tree->cde_name);
			return (exit(-1), -1);
		}
		else
			return (waitpid(pid, &status, WUNTRACED), test_signal(pid, status)); 
	}
}
