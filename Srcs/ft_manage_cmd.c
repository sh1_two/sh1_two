/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_manage_cmd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/16 20:04:46 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/30 17:54:10 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>
#include <errno.h>

void	error_management(t_env *env, int err, int ret)
{
	if (env->bfirst->right)
	{
		if (err != 0 && env->bfirst->right->error == 0)
			env->bfirst->right->error = err;
		if (env->bfirst->right->error == 0)
			env->bfirst->right->error = ret;
	}
}

int		manage_cmd(t_env *env, t_exec exec)
{
	static t_uchar		is_setup = 0;
	char				ret;
	t_btree				*n_tree;

	ret = 0;
	if (is_setup == 0)
		is_setup = ft_setup_pipeline(env->pipe);
	if (!(n_tree = env->bfirst->left))
	{
		ret = env->pipe[env->bfirst->operand](env, env->bfirst, NULL, exec);
		error_management(env, 0, ret);
	}
	else
	{
		ret = env->pipe[n_tree->operand](env, env->bfirst, n_tree, exec);
		error_management(env, 0, ret);
		n_tree = n_tree->left;
	}
	error_management(env, errno, ret);
	return (0);
}

char	*command_path(t_env *env, char *cmd)
{
	char	*path;
	char	**ptr;
	size_t	i;

	ptr = NULL;
	path = (ft_strchr(cmd, '/') != NULL) ? cmd : NULL;
	if (path == NULL && (ft_strncmp(cmd, "./", 2) == 0 || *cmd == '~'))
		path = ft_strjoin(ft_get_env(env->envp, "HOME"), cmd);
	i = 0;
	if (path == NULL && (ptr = ft_strsplit(ft_get_env(env->envp, "PATH"), ':')))
	{
		while (ptr[i] != NULL)
		{
			path = ft_multi_strjoin(3, ptr[i], "/", cmd);
			if (access(path, 0 | F_OK | X_OK) == 0)
				return (path);
			free(path);
			++i;
		}
		free(ptr);
	}
	else if (path != NULL && access(path, 0 | F_OK | X_OK) == 0)
		return (path);
	return (NULL);
}
