/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/14 05:28:44 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/19 03:45:32 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

int		ft_writefile(t_env *env, t_btree *r_part, int fd, unsigned int o_mod)
{
	char	*str;
	char	*args;
	int		fd_file;
	int		ret;

	str = NULL;
	args = ft_multi_strjoin(3, ft_get_env(env->envp, "PWD"),
													"/", r_part->cde_name);
	if ((fd_file = open(args, OPTION_FILE | o_mod, 0777)))
	{
		free(args);
		while ((ret = get_next_line(fd, &str)) > 0)
		{
			ft_putendl_fd(str, fd_file);
			free(str);
		}
		if (ret < 0)
			return (free(args), close(fd_file), -1);
	}
	else
		return (free(args), close(fd_file), -10);
	return (free(args), close(fd_file), 0);
}

int		ft_microshell(t_env *env, t_btree *r_part, int fd)
{
	char	*str;
	int		ret;

	ret = 1;
	str = NULL;
	while (ret > 0)
	{
		ft_printf("heredoc> ");
		if (((ret = get_next_line(STDIN, &str)) > 0) &&
				ft_strcmp(str, r_part->cde_name) == 0)
			return (0);
		ft_putendl_fd(str, fd);
		free(str);
	}
	return (ret);
}
