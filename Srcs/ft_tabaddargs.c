/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabaddargs.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 16:37:11 by agauci-d          #+#    #+#             */
/*   Updated: 2015/05/30 10:19:22 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

static int	ft_dst_fill(t_lst_bdin *dst, t_lst_bdin *src, char **tmp)
{
	int i;

	i = 0;
	while (dst->args_tab[i] != NULL)
		i++;
	tmp = (char **)malloc(sizeof(char *) * (i + 2));
	if (!tmp)
		return (RN_ERR("malloc() failed in ft_tabaddargs\n"), -1);
	tmp[i + 2] = NULL;
	tmp[i + 1] = NULL;
	tmp[i] = src->sauvegarde;
	src->sauvegarde = NULL;
	src->operand = o_empty;
	while (--i >= 0)
	{
		tmp[i] = dst->args_tab[i];
		dst->args_tab[i] = NULL;
	}
	dst->args_tab = tmp;
	return (0);
}

static int	ft_dst_empty_cmd(t_lst_bdin *dst, t_lst_bdin *src, char **tmp)
{
	tmp = (char **)malloc(sizeof(char *) * 2);
	if (!tmp)
		return (RN_ERR("malloc() failed in ft_tabaddargs\n"), -1);
	tmp[0] = src->cde_name;
	tmp[1] = NULL;
	dst->args_tab = tmp;
	return (0);
}

int			ft_tabaddargs(t_lst_bdin *dst, t_lst_bdin *src)
{
	char	**tmp;

	if (dst->args_tab != NULL)
		return (ft_dst_fill(dst, src, tmp));
	else
		return (ft_dst_empty_cmd(dst, src, tmp));
}
