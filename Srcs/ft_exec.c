/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/08 14:25:50 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/29 17:30:43 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

int			launch_cmd(t_env *env)
{
	t_btree	*save;
	int		ret;

	save = env->bfirst;
	errno = 0;
	ret = 0;
	while (env->bfirst != NULL)
	{
		if (ft_multi_strcmp(2, env->bfirst->cde_name, "exit", "quit") == 0)
			return (ft_clear_btree(&env->bfirst), exit(0), 0);
		else if ((ft_multi_strcmp(4, env->bfirst->cde_name,
					"cd", "env", "setenv", "unsetenv")) != 0)
		{
			if ((ret = manage_cmd(env, &execve)) != 0)
				return (0);
		}
		else
			ret = ft_execve(env, env->bfirst, env->envp);
		error_management(env, errno, ret);
		env->bfirst = env->bfirst->right;
	}
	if (save != NULL)
		ft_clear_btree(&save);
	return (0);
}

static int	ft_process(t_env *env)
{
	int		ret;
	t_btree	*environ;
	char	str[4096];

	environ = env->bfirst;
	env->bfirst = NULL;
	ft_set_env(&env->envp, "SHTXT",
			ft_nstrrchr((const char *)ft_get_pwd(), '/', 1));
	ft_printf("[%s%s%s] %s%s%s> ", C_BLUE, ft_get_env(env->envp, "NBR_SHELL"),
			C_NONE, C_CYAN, ft_get_env(env->envp, "SHTXT"), C_NONE);
	ret = read(STDIN, str, 4096);
	str[ret] = '\0';
	if (ret > 0 && ft_strisempty(str) != 0)
	{
		env->commande = 1;
		if (ft_parser(str, env) < 0)
			return (env->bfirst = NULL, 1);
		launch_cmd(env);
	}
	return (1);
}

static void	handle_sig(int sig)
{
	(void)sig;
	*SIGNAL = sig;
}

int			*singleton_signal(void)
{
	static int	signal;

	return (&signal);
}

void		ft_exec(t_env *env)
{
	signal(SIGINT, handle_sig);
	while (env->done)
	{
		env->commande = 0;
		*SIGNAL = 0;
		env->done = ft_process(env);
	}
}
