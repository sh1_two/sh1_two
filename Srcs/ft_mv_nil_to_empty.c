/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mv_nil_to_empty.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/15 15:16:18 by agauci-d          #+#    #+#             */
/*   Updated: 2015/05/21 11:28:59 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

void ft_mv_nil_to_empty(t_btree *tree)
{
	char	*empty[1];
	int		i;

	empty[0] = "\0";
	i = 0;
	if (!tree)
		return ;
	if (tree->left)
		ft_mv_nil_to_empty(tree->left);
	if (tree->args_tab == NULL)
		tree->args_tab = empty;
	if (tree->right)
		ft_mv_nil_to_empty(tree->right);
}
