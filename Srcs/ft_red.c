/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_red.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/29 18:03:20 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/29 18:09:49 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

int		ft_pipe(t_env *env, t_btree *cmd, t_btree *file, t_exec exec)
{
	int		fd_save[2];
	int		fd_pipe[2];

	pipe(fd_pipe);
	fd_save[0] = dup(STDOUT);
	fd_save[1] = dup(STDIN);
	if (fork() == 0)
	{
		close(fd_pipe[0]);
		dup2(fd_pipe[1], STDOUT);
		ft_exec_str(env, cmd, exec);
		exit(0);
	}
	else
	{
		close(fd_pipe[1]);
		dup2(fd_pipe[0], STDIN);
		ft_exec_str(env, file, exec);
		wait(NULL);
	}
	if (file->left)
		return (env->pipe[file->left->operand](env, file->left, NULL, exec));
	(dup2(fd_save[0], STDOUT), dup2(fd_save[1], STDIN));
	return (0);
}

int		ft_red_dl(t_env *env, t_btree *cmd, t_btree *file, t_exec exec)
{
	int		fd_save;
	int		fd_pipe[2];
	int		ret;
	char	str[4096];

	fd_save = dup(STDIN);
	pipe(fd_pipe);
	while (1)
	{
		ft_printf("%sheredoc>%s", BLUE, C_NONE);
		ret = read(STDIN, str, 4095);
		str[ret] = '\0';
		if (ft_strcmp(str, file->cde_name) == 10)
			break ;
		else
			ft_putstr_fd(str, fd_pipe[1]);
	}
	if (dup2(fd_pipe[0], STDIN) < 0)
		return (ft_printf("Cannot create dup!"), -1);
	(close(fd_pipe[1]), ft_exec_str(env, cmd, exec));
	if (file->left)
		return (env->pipe[file->left->operand](env, file->left, NULL, exec));
	if (dup2(fd_save, STDIN) < 0)
		return (ft_printf("Cannot create dup save!"), -1);
	return (close(fd_save), 0);
}

int		ft_red_l(t_env *env, t_btree *cmd, t_btree *file, t_exec exec)
{
	int		fd_file;
	int		fd_save;

	fd_save = dup(STDIN);
	if ((fd_file = open(file->cde_name, O_RDONLY)) < 0)
		return (RN_ERR("Cannot open file %s\n", file->cde_name), -1);
	if (dup2(fd_file, STDIN) < 0)
		return (RN_ERR("Cannot create dup!\n"), -1);
	close(fd_file);
	ft_exec_str(env, cmd, exec);
	if (file->left)
		return (env->pipe[file->left->operand](env, file->left, NULL, exec));
	if (dup2(fd_save, STDIN) < 0)
		return (RN_ERR("Cannot create dup save!\n"), -1);
	close(fd_save);
	return (0);
}

int		ft_red_dr(t_env *env, t_btree *cmd, t_btree *file, t_exec exec)
{
	int		fd_file;
	int		fd_save;

	fd_save = dup(STDOUT);
	if ((fd_file = open(file->cde_name, O_WRONLY | O_APPEND)) < 0)
		return (RN_ERR("Cannot open file %s\n", file->cde_name), -1);
	if (dup2(fd_file, STDOUT) < 0)
		return (RN_ERR("Cannot create dup!\n"), -1);
	close(fd_file);
	ft_exec_str(env, cmd, exec);
	if (file->left)
		return (env->pipe[file->left->operand](env, file->left, NULL, exec));
	if (dup2(fd_save, STDOUT) < 0)
		return (RN_ERR("Cannot create dup save!\n"), -1);
	close(fd_save);
	return (0);
}

int		ft_red_r(t_env *env, t_btree *cmd, t_btree *file, t_exec exec)
{
	int		fd_file;
	int		fd_save;

	fd_save = dup(STDOUT);
	if ((fd_file = open(file->cde_name, OPTION_FILE | O_TRUNC, 0644)) < 0)
		return (RN_ERR("Cannot open file %s\n", file->cde_name), -1);
	if (dup2(fd_file, STDOUT) < 0)
		return (RN_ERR("Cannot create dup!\n"), -1);
	close(fd_file);
	ft_exec_str(env, cmd, exec);
	if (file->left && file->left->operand <= o_red_dl)
		return (env->pipe[file->left->operand](env, file->left, NULL, exec));
	if (dup2(fd_save, STDOUT) < 0)
		return (RN_ERR("Cannot create dup save!\n"), -1);
	close(fd_save);
	return (0);
}
