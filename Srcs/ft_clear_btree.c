/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cleartree.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 14:34:00 by agauci-d          #+#    #+#             */
/*   Updated: 2015/05/29 16:55:02 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

void ft_clear_btree(t_btree **tree)
{
	t_btree	*tmp;
	int		i;

	tmp = *tree;
	if (!tree)
		return ;
	if (tmp->left != NULL)
		ft_clear_btree(&tmp->left);
	if (tmp->right != NULL)
		ft_clear_btree(&tmp->right);
	i = 0;
	while (tmp->args_tab != NULL && tmp->args_tab[i])
	{
		ft_strdel(&tmp->args_tab[i]);
		i++;
	}
	if (tmp->args_tab)
	{
		free(tmp->args_tab);
		tmp->args_tab = NULL;
	}
	free(tmp);
	*tree = NULL;
}
