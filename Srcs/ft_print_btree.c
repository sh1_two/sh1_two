/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_btree.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/29 17:58:43 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/29 18:01:33 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>
#include <stdio.h>

void	ft_print_btree(t_btree *tree)
{
	int		i;

	i = 0;
	if (!tree)
		return ;
	if (tree->left)
		ft_print_btree(tree->left);
	ft_putnbr_dl(tree->operand);
	if (tree->cde_name)
		ft_putendl(tree->cde_name);
	if (!tree->args_tab)
		ft_putendl("tree->args_tab == NULL");
	else
	{
		while (tree->args_tab && tree->args_tab[i] && tree->args_tab[i] != '\0')
		{
			ft_printf("args = %s\n", tree->args_tab[i]);
			i++;
		}
	}
	if (tree->right)
		ft_print_btree(tree->right);
}
