/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 17:38:12 by agauci-d          #+#    #+#             */
/*   Updated: 2015/05/30 14:25:05 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"
#include <stdlib.h>

t_lst_bdin *ft_lst_sh_new(t_operand oper, char *str)
{
	t_lst_bdin	*elem;

	elem = (t_lst_bdin *)malloc(sizeof(t_lst_bdin));
	if (!elem)
		return (NULL);
	if (elem)
	{
		ft_bzero((void *)elem, sizeof(t_lst_bdin));
		if (str != NULL)
			elem->sauvegarde = ft_strdup(str);
		else
			elem->sauvegarde = NULL;
		elem->cde_name = NULL;
		elem->operand = oper;
		elem->args_tab = NULL;
		elem->next = NULL;
		elem->prev = NULL;
	}
	return (elem);
}
