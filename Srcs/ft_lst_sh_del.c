/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_sh_del.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/25 17:38:13 by agauci-d          #+#    #+#             */
/*   Updated: 2015/05/10 21:12:27 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

void ft_lst_sh_del(t_lst_bdin **first)
{
	t_lst_bdin	*ptr;

	if (first && *first)
	{
		ptr = *first;
		if (ptr->next != NULL)
			ft_lst_sh_del(&(ptr->next));
		free((void *)ptr);
		*first = NULL;
	}
}
