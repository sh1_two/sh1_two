/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/30 23:06:02 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/30 23:19:45 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>

int main(int ac, char **av, char **envp)
{
	int		status;
	int		ret[3] = {0};
	pid_t	pid;

	status = 0;
	char *test[] = {"ls", "/", NULL};
	if ((pid = fork()) == 0)
	{
		execve("/bin/ls", test, envp);
		exit(-1);
	}
	else
	{
		waitpid(pid, &status, WUNTRACED);
		if (WIFEXITED(status))
		{
			ret[0] = WEXITSTATUS(status);
			printf("exit : %d\n", ret[0]);
		}
		else if (WIFSIGNALED(status))
		{
			ret[1] = WTERMSIG(status);
			printf("sig : %d\n", ret[0]);
		}
		else if (WIFSTOPPED(status))
		{
			ret[2] = status;
			printf("stop : %d\n", ret[2]);
		}
	}
	return (0);
}
